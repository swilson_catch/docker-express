# Express App

This repos should serve as an extensible boilerplate for any Express related work. It ships with:

 - Docker
 - Express
 - MongoDB
 - Mongoose

## Getting Started
You'll need Node, NPM and Docker installed on your local machine.

To run the app in development mode:
```
npm run start:docker
```

In Production the docker containers are not used.  It is assumed you would be hosting the app on a service like Heroku.
```
npm start
```

There's a prettier script in place for you convenience and to keep code neat and consistent.  You should probably run this before each commit...
```
npm run prettier
```

## Structure
The structure of this boilerplate app makes no real assumptions about what you'll do with it.  Therefore there is nothing more than the bare minimum in place to start serving a website/service/app using Express.  However there are some bits in place to help you get going.

### `/middleware`
Contains simple middleware functions to use in the app.  At the moment, middleware is only being used to force routes to be JSON, but you can conceivably write more to create auth or CORS protected routes etc...

### `/routes`
Contains some simple route handler examples and an `index.js` file to bootstrap the routes and pass them onto the app.  To keep the main app `index.js` file in the root of the repo clean, the app is passed to the route module and you should add more routes there as necessary

### `/db`
Contains a Mongoose JS connection function which returns a promise.  This will connect to a DB based on the `MONGODB_URI` environment variable.  There's no real need to add more to this directory, but you can if you wish or simply ignore/delete this directory if no DB is needed.

### `/models`
Contains a contrived Mongoose JS `Post` model.  You can add more models here, or simply ignore/delete this directory if no DB is needed.  Each model should use the model function to ensure all MongoDB collections contain the bare minimum properties:

 - `createdAt`
 - `modifiedAt`
