const mongoose = require('mongoose');

const db = () => {
  return new Promise((resolve, reject) => {
    mongoose.connection.on('err', reject);
    mongoose.connection.once('open', () => resolve(mongoose));
    mongoose.connect(process.env.MONGODB_URI);
  });
};

module.exports = db;
