require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');

const app = express();

// Ensure POST request bodies are easy to ingest in route handlers/middleware
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

// Bind routes
routes(app);

app.listen(process.env.PORT, () =>
  console.log('App running on port', process.env.PORT),
);
