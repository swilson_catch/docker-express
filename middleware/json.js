const middleware = (req, res, next) => {
  res.type('application/json');
  next();
};

module.exports = middleware;
