const db = require('../db');

const model = (name, schema) => {
  return new Promise((resolve, reject) => {
    db()
      .then(mongoose => {
        const timestampedSchema = new mongoose.Schema(schema, {
          timestamps: true,
        });

        resolve(mongoose.model(name, timestampedSchema));
      })
      .catch(reject);
  });
};

module.exports = model;
