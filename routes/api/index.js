const routeHandler = app => (req, res) => {
  res.send({
    response: 'JSON response...',
  });
};

module.exports = routeHandler;
