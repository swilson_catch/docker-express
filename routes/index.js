// Route middleware
const jsonMiddleware = require('../middleware/json');

// Route handlers
const homeRoute = require('./home');
const apiRoute = require('./api');

// Routes to be defined here
const routes = app => {
  // Home/index route
  app.get('/', homeRoute(app));

  // Contrived API route...
  app.get('/api', [jsonMiddleware], apiRoute(app));
};

module.exports = routes;
